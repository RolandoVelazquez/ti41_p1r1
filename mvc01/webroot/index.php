<?php

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));
define('WEBROOT', 'http://localhost:8080/DAII/R/mvc01');
define('VIEWS_PATH', ROOT.DS.'views');

require_once(ROOT.DS.'lib'.DS.'init.php');

App::run(str_replace("/DAII/R/mvc01","",$_SERVER['REQUEST_URI']));